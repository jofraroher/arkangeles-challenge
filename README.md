# Arkangeles Challenge

IOS application built in Swift from the Arkangeles Challenge.

## Overview

IOS application where the Marvel API is consumed to obtain the list of heroes, and shows the respective detail of each hero with the list of comics, events and series of each character.

## Installation

* Download the project.
* No need to load external third libraries.

## Heroes View

![alt text](img/heroes_list.png "Heroes List")

## Hero Details

![alt text](img/heroe_detail.png "Heroes detail")

## Comic Detail

![alt text](img/comic_detail.png "Comic detail")

## Functionality

![](gif/example.gif )

//
//  RequestTest.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import Foundation

import XCTest
@testable import Marvel

class RequestTests: XCTestCase {

    func testDownloadWebData() {
        let expectation = XCTestExpectation(description: "Download apple.com home page")
        let url = URL(string: "https://apple.com")!

        Request.shared.run(urlRequest: URLRequest(url: url)) { (data, _) in
            XCTAssertNotNil(data, "No data was downloaded.")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }
}

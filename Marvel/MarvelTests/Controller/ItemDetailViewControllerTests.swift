//
//  ItemDetailViewControllerTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest
@testable import Marvel

class ItemDetailViewControllerTests: XCTestCase {

    var itemDetailViewController: ItemDetailViewController!

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        itemDetailViewController = storyboard.instantiateViewController(withIdentifier: "ItemDetailViewController") as? ItemDetailViewController
        itemDetailViewController.itemTitle = "ItemTitle"
        itemDetailViewController.itemDescription = "ItemDescription"
        itemDetailViewController.image = #imageLiteral(resourceName: "imagePlaceholder")
        let _ = itemDetailViewController.view
    }

    func testIfViewIsInitialized() {
        XCTAssertEqual(itemDetailViewController.titleLabel.text, "ItemTitle")
        XCTAssertEqual(itemDetailViewController.descriptionLabel.text, "ItemDescription")
        XCTAssertEqual(itemDetailViewController.itemImageView.image, #imageLiteral(resourceName: "imagePlaceholder"))
    }


}

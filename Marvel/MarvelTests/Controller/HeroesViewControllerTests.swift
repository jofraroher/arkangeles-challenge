//
//  HeroesViewControllerTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest
@testable import Marvel

class HeroesViewControllerTests: XCTestCase {

    var heroesViewController: HeroesViewController!

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        heroesViewController = storyboard.instantiateViewController(withIdentifier: "HeroesViewController") as? HeroesViewController
        let _ = heroesViewController.view
    }

    func testIfOffsetInitialValueIsZero() {
        XCTAssertEqual(heroesViewController.offset, 0)
    }

    func testIfHeroesInitialValueIsEmpty() {
        XCTAssertTrue(heroesViewController.heroes.isEmpty)
    }

    func testIfIsSearchActiveInitialValueIsFalse() {
        XCTAssertFalse(heroesViewController.isSearchActive)
    }

    func testIfIsFavoritesActiveInitialValueIsFalse() {
        XCTAssertFalse(heroesViewController.isFavoritesActive)
    }

    func testIfEmptyLabelIsConfigured() {
        heroesViewController.configureEmptyLabel()
        XCTAssertEqual(heroesViewController.emptyLabel?.text, "No results.")
    }

}

//
//  ComicTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest
@testable import Marvel

class ComicTests: XCTestCase {

    var id: Int!
    var title: String!
    var itemDescription: String!
    var thumbnail: Thumbnail!
    var json: [String: Any]!

    override func setUp() {
        id = 100
        title = "Comic"
        itemDescription = "ComicDescription"
        thumbnail = Thumbnail(path: "path", imageExtension: "ext")
        json = ["id": 100,
                "title": "Comic",
                "description": "ComicDescription",
                "thumbnail": ["path": "path",
                              "extension": "ext"]
               ]
    }

    func testComicCreationWithDefaultInitializer() {
        let comic = Comic(id: id, title: title, description: description, thumbnail: thumbnail)
        XCTAssertEqual(comic.id, id)
        XCTAssertEqual(comic.title, title)
        XCTAssertEqual(comic.description, description)
        XCTAssertEqual(comic.thumbnail?.path, thumbnail.path)
        XCTAssertEqual(comic.thumbnail?.imageExtension, thumbnail.imageExtension)
    }

    func testComicCreationWithJSON() {
        let comic = Comic(json: json)
        XCTAssertEqual(comic?.id, id)
        XCTAssertEqual(comic?.title, title)
        XCTAssertEqual(comic?.description, itemDescription)
        XCTAssertEqual(comic?.thumbnail?.path, thumbnail.path)
        XCTAssertEqual(comic?.thumbnail?.imageExtension, thumbnail.imageExtension)
    }

}

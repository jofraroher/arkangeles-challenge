//
//  EventTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest

import XCTest
@testable import Marvel

class EventTests: XCTestCase {

    var id: Int!
    var title: String!
    var itemDescription: String!
    var thumbnail: Thumbnail!
    var json: [String: Any]!

    override func setUp() {
        id = 100
        title = "Event"
        itemDescription = "EventDescription"
        thumbnail = Thumbnail(path: "path", imageExtension: "ext")
        json = ["id": 100,
                "title": "Event",
                "description": "EventDescription",
                "thumbnail": ["path": "path",
                              "extension": "ext"]
               ]
    }

    func testEventCreationWithDefaultInitializer() {
        let event = Event(id: id, title: title, description: description, thumbnail: thumbnail)
        XCTAssertEqual(event.id, id)
        XCTAssertEqual(event.title, title)
        XCTAssertEqual(event.description, description)
        XCTAssertEqual(event.thumbnail?.path, thumbnail.path)
        XCTAssertEqual(event.thumbnail?.imageExtension, thumbnail.imageExtension)
    }

    func testEventCreationWithJSON() {
        let event = Event(json: json)
        XCTAssertEqual(event?.id, id)
        XCTAssertEqual(event?.title, title)
        XCTAssertEqual(event?.description, itemDescription)
        XCTAssertEqual(event?.thumbnail?.path, thumbnail.path)
        XCTAssertEqual(event?.thumbnail?.imageExtension, thumbnail.imageExtension)
    }

}


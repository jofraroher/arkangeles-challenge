//
//  MarvelServiceTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest
@testable import Marvel

class MarvelServiceTests: XCTestCase {

    func testFetchHeroes() {
        let expectation = XCTestExpectation(description: "Fetch heroes from Marvel API")
        HeroService.fetchHeroes(0) { result in
            switch result {
            case .success(_):
                XCTAssert(true)
            case .failure(_):
                XCTFail("Failure retrieving heroes")
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }

}

//
//  FavoriteServiceTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest
@testable import Marvel

class FavoriteServiceTests: XCTestCase {

    var hero: Hero!

    override func setUp() {
        hero = Hero(id: 1, name: "Hulk", thumbnail: nil)
    }

    func testIfHeroIsAdded() {
        FavoritesService.add(hero)
        XCTAssertTrue(FavoritesService.favorites.contains(hero))
    }

    func testIfHeroIsDeleted() {
        FavoritesService.remove(hero)
        XCTAssertFalse(FavoritesService.favorites.contains(hero))
    }

    func testIfItIsFavorite() {
        FavoritesService.add(hero)
        XCTAssertTrue(FavoritesService.isFavorite(hero))
    }

    func testIfItIsNotFavorite() {
        FavoritesService.remove(hero)
        XCTAssertFalse(FavoritesService.isFavorite(hero))
    }
}

//
//  ImageServiceTests.swift
//  MarvelTests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest
@testable import Marvel

class ImageServiceTests: XCTestCase {

    var imageService: ImageService!

    override func setUp() {
        imageService = ImageService()
    }

    func testDownloadImage() {
        let url = "https://www.google.com.br/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"
        let expectation = XCTestExpectation(description: "Downloading image")
        imageService.downloadImage(fromURL: URL(string: url)!) { image in
            XCTAssertNotNil(image)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }

    func testSavingAndRetrievingImage() {
        imageService.setImage(image: #imageLiteral(resourceName: "imagePlaceholder"), forkey: "placeholderImage")
        let image = imageService.imageForKey(key: "placeholderImage")
        XCTAssertNotNil(image)
    }

}

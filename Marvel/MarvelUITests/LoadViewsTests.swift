//
//  LoadViewsTests.swift
//  MarvelUITests
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import XCTest

class LoadViewsTests: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    func testShowHeroDetails() {
        app.tables["HeroesTableView"].cells.firstMatch.tap()
        XCTAssertTrue(app.otherElements["DetailsView"].exists)
    }

    func testIfLoadedHeroes() {
        let cells = app.tables["HeroesTableView"].cells
        let notEmpty = NSPredicate(format: "count > 0")
        expectation(for: notEmpty, evaluatedWith: cells, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(cells.count > 0)
    }

}

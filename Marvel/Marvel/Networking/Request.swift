//
//  Request.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import Foundation

class Request {

    static let shared = Request()
    private let session = URLSession(configuration: .default)
    typealias Response = ((Data?, Error?) -> ())

    func run(urlRequest: URLRequest, completion: @escaping Response) {
        session.dataTask(with: urlRequest) { data, _, error in
            completion(data, error)
        }.resume()
    }

}

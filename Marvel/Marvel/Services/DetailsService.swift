//
//  DetailsService.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import Foundation

class DetailsService {

    class func fetchDetails<T: Parseable>(type: T.Type, heroID: String, completion: @escaping (Result<[T]>) -> Void) {
        Request.shared.run(urlRequest: urlRequest(forType: type, heroID: heroID)) { data, error in
            completion(T.parseResponse(data: data, error: error))
        }
    }

    private class func urlRequest<T: Parseable>(forType type: T.Type, heroID: String) -> URLRequest {
        if type is Comic.Type {
            return HeroAPI.comics(heroID: heroID).asURLRequest()
        } else if type is Event.Type {
            return HeroAPI.events(heroID: heroID).asURLRequest()
        } else if type is Serie.Type {
            return HeroAPI.series(heroID: heroID).asURLRequest()
        }
        return HeroAPI.stories(heroID: heroID).asURLRequest()
    }

}

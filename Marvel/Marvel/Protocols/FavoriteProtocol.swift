//
//  FavoriteProtocol.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import UIKit

protocol FavoriteProtocol: class {
    func didRemoveHero(atIndexPath indexPath: IndexPath)
}

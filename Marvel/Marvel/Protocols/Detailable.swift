//
//  Detailable.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

protocol Detailable {
    var id: Int { get set }
    var title: String { get set }
    var description: String { get set }
    var thumbnail: Thumbnail? { get set }
}

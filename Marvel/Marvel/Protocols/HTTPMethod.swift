//
//  HTTPMethod.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

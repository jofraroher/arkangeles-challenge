//
//  DetailsViewController+Delegate.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import UIKit

extension DetailsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let detailsTableViewCell = cell as? DetailsTableViewCell else { return }
        detailsTableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.section)

        if !didFetch[indexPath.section] {
            fetchDetails(forSection: Sections(rawValue: indexPath.section))
        }
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).backgroundView?.backgroundColor = UIColor.red
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case Sections.comics.rawValue:
            return "Comics"
        case Sections.events.rawValue:
            return "Events"
        case Sections.series.rawValue:
            return "Series"
        case Sections.stories.rawValue:
            return "Stories"
        default:
            return ""
        }
    }
}

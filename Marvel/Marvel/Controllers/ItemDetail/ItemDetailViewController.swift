//
//  ItemDetailViewController.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import UIKit

class ItemDetailViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    // MARK: - Attributes

    var itemTitle: String?
    var itemDescription: String?
    var image: UIImage?

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        title = itemTitle
        titleLabel.text = itemTitle
        descriptionLabel.text = itemDescription
        itemImageView.image = image
    }

    // MARK: - Navigation

    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}

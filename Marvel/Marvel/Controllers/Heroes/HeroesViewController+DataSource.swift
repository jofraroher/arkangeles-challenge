//
//  HeroesViewController+DataSource.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import UIKit

extension HeroesViewController: UITableViewDataSource, FavoriteProtocol {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFavoritesActive ? FavoritesService.favorites.count : heroes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeroCell", for: indexPath) as! HeroTableViewCell
        let hero = isFavoritesActive ? FavoritesService.favorites[indexPath.row] : heroes[indexPath.row]
        cell.clean()
        cell.delegate = self
        cell.hero = hero
        return cell
    }

}

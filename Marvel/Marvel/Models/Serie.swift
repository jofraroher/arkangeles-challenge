//
//  Serie.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import Foundation

struct Serie: Detailable {

    var id: Int
    var title: String
    var description: String
    var thumbnail: Thumbnail?

}

extension Serie: Parseable {

    init?(json: [String: Any]) {
        guard let id = json["id"] as? Int,
            let title = json["title"] as? String,
            let description = json["description"] as? String else {
                return nil
        }

        var thumbnail: Thumbnail?
        if let thumbnailJSON = json["thumbnail"] as? [String: Any],
            let downloadedThumbnail = Thumbnail(json: thumbnailJSON) {
            thumbnail = downloadedThumbnail
        }

        self.init(id: id, title: title, description: description, thumbnail: thumbnail)
    }

}

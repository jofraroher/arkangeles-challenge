//
//  Thumbnail.swift
//  Marvel
//
//  Created by Jose Francisco Rosales Hernandez on 11/03/21.
//  Copyright © 2021 Jose Francisco Rosales Hernandez. All rights reserved.
//

import Foundation

struct Thumbnail: Codable {

    let path: String
    let imageExtension: String
    var fullPath: String {
        return [path, imageExtension].joined(separator: ".")
    }

}

extension Thumbnail: Parseable {

    init?(json: [String: Any]) {
        guard let path = json["path"] as? String,
            let imageExtension = json["extension"] as? String else {
                return nil
        }
        self.init(path: path, imageExtension: imageExtension)
    }

}
